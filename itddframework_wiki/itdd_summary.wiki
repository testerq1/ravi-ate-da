= 什么是自动化测试？ =
*Testing Cycle*: Config，Inject，Check，Restore

*自动化测试*是对这个Cycle代码化

= Summary =
iTDD可以为接口级仿真测试提供开放统一的框架(可比较[https://code.google.com/p/robotframework/ Robot Framework])，典型的如
== 企业级应用interface ==
 [http://www.infoq.com/resource/articles/atdd-from-the-trenches/en/resources/fig5small.jpg]

== 网络 ==
  * [http://en.wikipedia.org/wiki/IP_Multimedia_Subsystem IMS]
   [http://www.radio-electronics.com/info/telecommunications_networks/ims-ip-multimedia-subsystem/ims-architecture-simplified-01.gif]
  * [http://en.wikipedia.org/wiki/Software-defined_networking SDN]
   [https://www.opennetworking.org/images/stories/sdn-resources/meet-sdn/sdn-3layers.gif]   
  * [http://en.wikipedia.org/wiki/Multiprotocol_Label_Switching MPLS]
   [http://www.h3c.com/portal/res/200705/31/20070531_107768_image008_201198_57_0.gif]

== Mobile APPs ==
 * [https://code.google.com/p/robotium/ Robotium]
 * [http://appium.io/zh-cn/index.html Appium]

======
======
= Concepts and Features =
== 四种模式 ==
iTDD可用4种方式使用：
 * TestNG模式：在此种模式下，脚本文件被当作TestNG框架的用例类。itdd保留了大多数TestNG的功能，但简化了使用方法，加强了调试功能和日志功能，更重要的是实现了测试数据和测试逻辑的分离，可以去继承测试逻辑
 * Groovy模式：在此种模式下，文件被当作普通的Groovy脚本来解释。itdd用约定的方式完成3rd-party库的加载，你可以不用处理classpath问题，放进去就可以用了。这里的库可以是jar，也可以是脚本
 * Shell模式：此模式会大量使用Java的反射技术，为整个itdd加一个CLI的壳，暂时只完成了基础的部分
 * Server模式：此模式使用Spring+Camel+JMQ实现，是为搭建一个多用途的测试工厂做准备的，暂时只完成了基础的部分

== 四个核心概念 ==
 * 测试床，测试床与脚本分离
 * 测试套件，测试用例，测试逻辑与数据分离
 * UA/URI，能力集，插件式，[http://tools.ietf.org/html/rfc3986 URI]代表资源及配置
 * [http://www.pairwise.org/ 数据组合测试]与随机测试

== 三个主要父类 ==
 * Testbed
 * Testcase类
 * AbstractURIHandler

== xUA ==
 * 协议类UA
 * Terminal类UA
 * 数据库类UA
 * 封装类UA
   # [http://www.seleniumhq.org/ selenium/webdriver]
   # [https://code.google.com/p/robotium/ Robotium]
   # ...
====

== [http://testng.org/doc/index.html TestNG] ==
 * 白盒测试，黑盒测试及其中间地带的测试
 * 多层次测试床
 * 测试报告接口
 * iTDD对TestNG的增删


== Network ==
 * [http://mina.apache.org/ Apache mina]
 * [http://netty.io/ Netty]
 * [http://jnetpcap.com/ Pcap]
 * [http://www.openss7.org/ SS7]

== Databases ==
 * 关系型
 * NoSQL


== OS ==
 * Host父类
 * Linux、Windows and NTools

== 并行与分布式 ==
 * TestNG parallel, Master/Slave
 * Google SSH
 * [http://hadoop.apache.org/ MapReduce] and [http://gpars.codehaus.org/ Gpars]（TODO）


== String ==
 * Expect
 * 正则表达式
 * X2X和GenData类

== 工具 ==
 * [http://nmap.org/ Nmap]
 * [http://www.gns3.net/dynamips/ Dynamips]
 * OS及跨OS
 * File system
 * 其他

== 与其他系统的整合 ==
 * 我的DA[https://code.google.com/p/ravi-ate-da/ 桌面助手]工具
 * [http://jenkins-ci.org/ Jenkins]/[http://hudson-ci.org/ Hudson]
 * 通知
 * [http://www.teamst.org/ Testlink]
 * OAuth
 * [https://www.atlassian.com/software/jira Jira]


== 日志、脚本及用例维护 ==
 * 我的DA[https://code.google.com/p/ravi-ate-da/ 桌面助手]工具
 * [https://code.google.com/p/qrfciewer-win64/ RFC格式的日志]
 * 关于测试用例维护的一些考虑
 * [http://logging.apache.org/log4j/1.2/ Log4j]